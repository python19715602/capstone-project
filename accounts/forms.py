from django import forms
from django.contrib.auth.models import User 
from django.contrib.auth.forms import UserCreationForm, UsernameField
from django.contrib.auth import get_user_model
from .models import Profile 

class MyUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        ]
        widgets = {
            'username': forms.TextInput(attrs={'class':'form-control w-50'}),
            'first_name': forms.TextInput(attrs={'class':'form-control w-50'}),
            'last_name': forms.TextInput(attrs={'class':'form-control w-50'}),
            'email': forms.EmailInput(attrs={'class':'form-control w-50'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name in ['password1','password2']:
            print(self.fields[field_name].widget.attrs.update({'class':'form-control w-50'}))



class UserEditForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name','email']
        labels = {
            'first_name': 'First name',
            'last_name': 'Last name',
            'email': 'Email'
        }
        widgets = {
            'first_name': forms.TextInput(attrs={'class':'form-control'}),
            'last_name': forms.TextInput(attrs={'class':'form-control'}),
            'email': forms.EmailInput(attrs={'class':'form-control'}),
        }

class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ['user']
        widgets = {
            'birth_date': forms.DateInput(attrs={'class':'form-control'}),
            'avatar': forms.FileInput(attrs={'class':'form-control'}), 
        }
        

class PasswordChangeForm(forms.ModelForm):
    password = forms.CharField(
        label='New password',
        strip=False,
        widget=forms.PasswordInput(attrs={'class':'form-control'}))

    password_confirm = forms.CharField(
        label='Confirm password',
        strip=False,
        widget=forms.PasswordInput(attrs={'class':'form-control'}))

    old_password = forms.CharField(
        label='Old password',
        strip=False,
        widget=forms.PasswordInput(attrs={'class':'form-control'}))

    def clean_password_confirm(self):
        password = self.cleaned_data.get('password')
        password_confirm = self.cleaned_data.get('password_confirm')

        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError('Passwords do not match!')
        return password_confirm

    def clean_old_password(self):
        old_password = self.cleaned_data.get('old_password')

        if not self.instance.check_password(old_password):
            raise forms.ValidationError('Old password id incorrect!')


    def save(self, commit =True):
        user = self.instance
        user.set_password(self.cleaned_data.get('password'))
        if commit:
            user.save()
        return user

    class Meta:
        model= get_user_model()
        fields = ['old_password', 'password', 'password_confirm']
