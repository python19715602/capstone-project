from django.views.generic import View, TemplateView
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

class IndexView(TemplateView):
    template_name = 'index.html'



@login_required #не дает зайти пока не залогинешься 
def my_view(request):
    return render(request, 'index.html')