from django.shortcuts import render, redirect, get_object_or_404
from django.templatetags.static import static 
from django.urls import reverse, reverse_lazy 
from ..models import Post, Title
from ..forms import PostForm, CommentForm, SearchForm
from django.views.generic import View, FormView, TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from ..helpers.views import CustomFormView, CustomListView, CustomDetailView, CustomCreateView, CustomUpdateView, CustomDeleteView
from django.http import HttpRequest, HttpResponse
from django.utils.http import urlencode 
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect


class PostListView(ListView):
    template_name = 'posts/list.html'
    model = Post
    context_object_name = 'posts'
    form = SearchForm
    
   

    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)

    def get_search_form(self):
        return self.form(self.request.GET)


    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data.get('search')
        return None

    def get_context_data(self, *, object_list =None, **kwargs):
        context = super().get_context_data(object_list = object_list, **kwargs)
        context['form']=self.form
        if self.search_value:
            context['query'] = urlencode({'search':self.search_value})
        return context 


    def get_queryset(self):
        queryset = super().get_queryset()
        if self.search_value:
            query = Q(composition__icontains = self.search_value)
            queryset = queryset.filter(query)
        return queryset



class PostDetailView(DetailView):
    template_name = 'posts/detail.html'
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        comment_form = CommentForm
        my_post = self.object
        comments = my_post.comments.order_by('-created_at')
        context['comments'] = comments
        context['comment_form'] = comment_form
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        action = request.POST.get('action')
        if action == 'increment_interest':
            self.object.interest_count += 1
            self.object.save()
        return HttpResponseRedirect(self.request.path_info)


class PostCreateView(LoginRequiredMixin,CreateView):
    template_name = 'posts/create.html'
    model = Post
    form_class = PostForm

    # def dispatch(self, request, *args, **kwargs):
    #     if not request.user.is_authenticated:
    #         return redirect('login')
    #     return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


    def get_success_url(self):
        return reverse('post_detail', kwargs={'pk':self.object.pk})



            
        
class PostUpdateView(PermissionRequiredMixin, UpdateView):
    model = Post
    template_name = 'posts/update.html'
    form_class = PostForm
    context_object_name = 'post'
    permission_required = 'TTE.change_post'


    # def dispatch(self, request, *args, **kwargs):
    #     user = request.user
    #     if not user.is_authenticated:
    #         return redirect('home_page')

    #     if not user.has_perm('TTE.change_post'):
    #         raise PermissionDenied 
    #     return super().dispatch(request, *args, **kwargs)




    def get_success_url(self):
        return reverse('post_detail', kwargs={'pk':self.object.pk})

    


  


class PostDeleteView(DeleteView):
    template_name = 'posts/delete.html'
    model = Post
    context_object_name = 'post'
    success_url = reverse_lazy('post_list')



