from django.shortcuts import render, redirect
from ..models import Title
from ..forms import TitleForm
from django.views.generic import View, ListView, CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin



class TitleListView(ListView):
    template_name = 'titles/list.html'
    model = Title
    context_object_name = 'titles'
    ordering = ['name']
    paginate_by = 5
    paginate_orphans = 1

    
class TitleCreateView(LoginRequiredMixin,CreateView):
    template_name = 'titles/create.html'
    model = Title 
    form_class = TitleForm
    success_url = reverse_lazy('title_list')


class TitleDeleteView(DeleteView):
    model = Title
    success_url = reverse_lazy('title_list')

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)


class TitleUpdateView(UpdateView):
    model = Title
    template_name = 'titles/update.html'
    form_class = TitleForm
    context_object_name = 'title'
    success_url = reverse_lazy('title_list')
    