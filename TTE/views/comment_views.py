from django.shortcuts import get_object_or_404, redirect
from ..forms import CommentForm
from ..helpers.views import CustomCreateView, CustomUpdateView, CustomDeleteView
from ..models import Comment, Post
from django.urls import reverse 
from django.views.generic import CreateView, UpdateView, DeleteView

class CommentCreateView(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'comments/create.html'

    def form_valid(self, form):
        my_post = get_object_or_404(Post, pk = self.kwargs.get('post_pk'))
        comment = form.save(commit=False)
        comment.post = my_post
        comment.author = self.request.user
        comment.save()
        return redirect('post_detail', pk=my_post.pk)


 
class CommentUpdateView(UpdateView):
    model = Comment 
    template_name = 'comments/update.html'
    context_object_name = 'comment'
    form_class = CommentForm
    pk_url_kwarg= 'post_pk'

    def get_success_url(self):
        return reverse('post_detail', kwargs={'pk':self.object.post.pk})



class CommentDeleteView(DeleteView):
    model = Comment 
    pk_url_kwarg = 'post_pk'


    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('post_detail', kwargs={'pk': self.object.post.pk})



   