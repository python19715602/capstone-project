from django.urls import path
from ..views.title_views import (
    TitleListView, 
    TitleCreateView,
    TitleUpdateView,
    TitleDeleteView,
    )


urlpatterns =[
    path('create/', TitleCreateView.as_view(), name='title_create'),
    path('list/', TitleListView.as_view(), name='title_list'),
    path('<int:pk>/update',TitleUpdateView.as_view(), name ='title_update'),
    path('<int:pk>/delete',TitleDeleteView.as_view(), name ='title_delete'),
]