from django.contrib import admin
from .models import Post, Tag




class PostAdmin(admin.ModelAdmin):
    list_display=['title', 'description', 'composition', 'author', 'created_at']
    list_filter=['title']
    search_fields=['title', 'composition']
    fields=['title', 'description', 'composition', 'author', 'created_at']
    readonly_fields=['created_at']


class TagAdmin(admin.ModelAdmin):
    fields = ['name']



admin.site.register(Post, PostAdmin)
admin.site.register(Tag, TagAdmin)
