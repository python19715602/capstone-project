# Generated by Django 5.0.4 on 2024-05-06 18:14

from django.db import migrations


def transfer_tags(apps, schema_editor):
    Post = apps.get_model('TTE.Post')
    for post in Post.objects.all():
        post.tags.set(post.tags_old.all())

def rollback_transfer(apps, schema_editor):
    Post = apps.get_model('TTE.Post')
    for post in Post.objects.all():
        post.tags_old.set(post.tags.all())


class Migration(migrations.Migration):

    dependencies = [
        ('TTE', '0007_post_tags_alter_post_tags_old'),
    ]

    operations = [
        migrations.RunPython(transfer_tags, rollback_transfer)

    ]
