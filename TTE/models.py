from django.db import models
from .helpers.validators import at_least_3
from django.contrib.auth import get_user_model


class Title(models.Model):
    name=models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name='Название продукта'
    )

    def __str__(self) -> str:
        return self.name

    
class Post(models.Model):
    title = models.ForeignKey(
        to=Title,
        on_delete=models.CASCADE,
        null=False, 
        verbose_name='Название'
        )

    description = models.TextField(
        max_length=3000,
        null=True,
        blank=True,
        verbose_name='Описание'
        )

    composition = models.TextField(
        max_length=3000,
        null=True,
        blank=True,
        verbose_name='Состав',
        validators=[at_least_3]
    )

    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_DEFAULT,
        default=1,
        null=True,
        verbose_name='Автор',
        related_name='posts'
    )

    tags= models.ManyToManyField(
        'TTE.Tag',
        related_name='posts',
        blank=True,
        verbose_name='Тэги'
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания'
    )
    interest_count = models.IntegerField(
        default=0, 
        verbose_name='Количество интересов')


    def __str__(self):
        return f"{self.pk} - {self.title}"

    class Meta:
        permissions = [
            ('can_read_post', 'Can read post')
        ]

class Comment(models.Model):
    text= models.TextField(
        max_length=1000,
        null=False,
        blank=False,
        verbose_name='Комментарий'
    )
    author = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        default="Аноним",
        verbose_name='Автор'
    )
    post = models.ForeignKey(
        'TTE.Post', 
        on_delete=models.CASCADE, 
        related_name='comments', 
        verbose_name='Post'
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания'
    )
    update_at = models.DateTimeField(
        auto_now=True,
        verbose_name='Дата редактирования'
    )

    def __str__(self) -> str:
        return self.text[:20]


class Tag(models.Model):
    name=models.CharField(
        max_length=30,
        verbose_name='Тэг'
    )
    created_at=models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания'
    )

    def __str__(self) -> str:
        return self.name


