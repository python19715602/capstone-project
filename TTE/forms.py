from django import forms
from django.forms import widgets, ValidationError
from .models import Title, Tag, Post, Comment
from .helpers.validators import at_least_3

class PostForm(forms.ModelForm):


    class Meta:
        model = Post
        exclude = ['author']
        widgets = {
            'title': forms.Select(attrs={
                'class':'form-select',
                'placeholder': 'Название'}),
            'description': forms.Textarea(attrs={
                'class':'form-control', 
                'rows':'4'}),
            'composition': forms.Textarea(attrs={
                'class':'form-control', 
                'rows':'4'}),
            'author': forms.Textarea(attrs={
                'class':'form-control', 
                }),
            'tags': forms.SelectMultiple(attrs={'class': 'form-select'})}
        





    def clean(self):
        cleaned_data = super().clean()


        if cleaned_data.get('description') == cleaned_data.get('composition'):
            raise ValidationError("Description and Composition should not be similar!")
        return cleaned_data



    def clean_description(self):
        description = self.cleaned_data.get('description')
        if len(description) < 10:
            raise ValidationError('Description is too short!')
        return description

    


class TitleForm(forms.ModelForm):
    class Meta:
        model = Title
        fields = ['name']
        widgets = {'name': forms.TextInput(attrs={
            'class':'form-control',
            'placeholder': 'Название'
        })}

    


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']
        widgets = {
            'text': forms.Textarea(attrs={
                'class': 'form-control'
            }),
            'author': forms.TextInput(attrs={
                'class': 'form-control'
            })
        }




class SearchForm(forms.Form):
    search = forms.CharField(
        max_length=100, 
        required=False, 
        label='Search')