from django.apps import AppConfig


class TteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'TTE'
